# borg-backup-script
A script to automate Borg daily backups.

# Description
A simple script to complete the automation of Borg remote, compressed and encrypted backups. It assumes the communication with a remote machine with Borg installed, hosting a specific backup disk (UUID identification).
It performs a number of checks before the actual backup, namely:
- Checks if the previous backup procedure has finished. If not, sends a message and cancels the operation.
- Checks if the backup disk is in place on the remote machine, using the UUID. If not, sends a message and cancels the operation.
- Checks if the backup partition is already mounted, this is assumed as not desireable. If so, sends a notice and continues the operation.

# Usage
One can issue a regular backup plan using crontab entries (more examples here: https://crontab.guru/), like the following:
```
# m h  dom mon dow         command
  0 1  *   *   TUE-SAT     /bin/bash /root/scripts/remote-backup/remote-backup.sh >/dev/null 2>&1
```
Multiple copies of the script (and their corresponding crontab entries) can be used to automate different backup procedures.

The script has a "Values to fill/change" section, like this:
```
# Values to fill/change
###########################################################
EMAIL="admin@example.com" # notification email recipient
PREFIX="SambaFiles" # prefix for the archive name
BACKUPDATE=`date --date='1 days ago' +%Y%m%d` # date command format for the archive name
SSHHOST="remotebackup" # SSH passwordless hostname
MOUNTPATH="/mnt/disks/usb" # backup disk mount path
RPATH="/backup" # repository path inside backup disk
PASSPATH="/root/.secret" # full path to the file containing the repository encryption key
BACKUPDIR="/mnt/sambafiles" # directory to backup
KEEPDAILY=7 # archives to keep daily
KEEPWEEKLY=4 # archives to keep weekly
KEEPMONTHLY=2 # archives to keep monthly
BACKUPUUID="9467f4de-4231-401f-bcaa-fee718d49e85" # UUID of the backup disk used
WARNINGMSG="WARNING: Automated backup failed!" # warning notification message subject
LOGDIR=/var/log/backup # directory to put log files
###########################################################
```
Values are mostly self explanatory.
The script assumes that there is an initialized Borg repository inside `$RPATH`, in the disk with UUID: `$BACKUPUUID`, which will be properly mounted to `$MOUNTPATH` by the script.
