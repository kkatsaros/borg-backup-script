#!/bin/bash

#================================================================
# HEADER
#================================================================
#% USAGE
#+    Usually at a daily/weekday cron job.
#%
#% DESCRIPTION
#%    A simple script to automate Borg daily backups.
#%
#% REFERENCES
#%    Inspiration and code from these two excelent articles:
#%      http://borgbackup.readthedocs.io/en/stable/deployment/automated-local.html
#%      https://opensource.com/article/17/10/backing-your-machines-borg
#%    Thanks to Borg and Christopher Aedo.
#%
#% OPTIONS
#%    No option available yet.
#%
#================================================================
#- IMPLEMENTATION
#-    version         0.1
#-    author          Kostas Katsaros
#-                    (https://gitlab.com/kkatsaros/)
#-    copyright       Copyright 2018 (c) Kostas Katsaros
#-    license         GNU General Public License (GPL) 3.0
#-                    (https://www.gnu.org/licenses/gpl-3.0.en.html)
#-    issue tracking  https://github.com/kkatsaros/borg-backup-script
#-
#================================================================
#  HISTORY
#     2018/08/09 : kkatsaros : Script creation
# 
#================================================================
# END_OF_HEADER
#================================================================

# Values to fill/change
###########################################################
EMAIL="admin@example.com" # notification email recipient
PREFIX="SambaFiles" # prefix for the archive name
BACKUPDATE=`date --date='1 days ago' +%Y%m%d` # date command format for the archive name
SSHHOST="remotebackup" # SSH passwordless hostname
MOUNTPATH="/mnt/disks/usb" # backup disk mount path
RPATH="/backup" # repository path inside backup disk
PASSPATH="/root/.secret" # full path to the file containing the repository encryption key
BACKUPDIR="/mnt/sambafiles" # directory to backup
KEEPDAILY=7 # archives to keep daily
KEEPWEEKLY=4 # archives to keep weekly
KEEPMONTHLY=2 # archives to keep monthly
BACKUPUUID="9467f4de-4231-401f-bcaa-fee718d49e85" # UUID of the backup disk used
WARNINGMSG="WARNING: Automated backup failed!" # warning notification message subject
LOGDIR=/var/log/backup # directory to put log files
###########################################################

if [ ! -d "$LOGDIR" ]
then
    mkdir "$LOGDIR" > /dev/null
fi

exec >$LOGDIR/remote-backup-daily-`date --date='1 days ago' +"%F_%a"`.log

# Checks
echo "`date` Starting checks..."
## Check if borg is already running, maybe previous run didn't finish.
if pidof -x borg >/dev/null
then
    echo "`date` WARNING: Another process running, exiting."
    EMAILSUBJECT="$WARNINGMSG Another process running."
    EMAILBODY="At `date` an automated remote backup attemted, but another remote backup process was already running (PID=`pidof -x borg`). The attemt was cancelled. Please, contact an administrator to check if that\'s ok."
    echo $EMAILBODY|mail -s "$EMAILSUBJECT" $EMAIL
    exit
else
    echo "`date` Check Ok: Bord is not running."
fi
## Check if disk is present.
if ssh $SSHHOST test -e /dev/disk/by-uuid/$BACKUPUUID
then
    echo "`date` Check Ok: backup disk present."
else
    echo "`date` WARNING: Backup disk NOT present, exiting."
    EMAILSUBJECT="$WARNINGMSG No disk found."
    EMAILBODY="At `date` an automated remote backup attemted, but the backup disk was NOT found present. The attemt was cancelled. Please, contact an administrator to check if that\'s ok."
    echo $EMAILBODY|mail -s "$EMAILSUBJECT" $EMAIL
    exit
fi
## Mount backup disk - check if partition is mounted first
MP=`ssh $SSHHOST blkid -U $BACKUPUUID`
MCHECK=`ssh $SSHHOST mount | grep "$MP"`
if [ "$MCHECK" != "" ]
then
    echo "`date` NOTICE: Backup disk already mounted."
    EMAILSUBJECT="NOTICE: Automated backup needs attention."
    EMAILBODY="At `date` an automated remote backup attemted, yet the backup disk was already mounted. If this message insists, please contact an administrator. The attemt continued, expect another message about the outcome."
    echo $EMAILBODY|mail -s "$EMAILSUBJECT" $EMAIL
else
    echo "`date` Check Ok: backup disk is not mounted."
    ssh $SSHHOST mount /dev/disk/by-uuid/$BACKUPUUID $MOUNTPATH
fi
echo "`date` All checks finished, proceeding with backup."

# Init borg
REPOSITORY="$SSHHOST:$MOUNTPATH$RPATH"
PASSPHRASE=`cat $PASSPATH`
export BORG_PASSPHRASE=$PASSPHRASE #sets passwordless procedure
export BORG_RELOCATED_REPO_ACCESS_IS_OK=no
export BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=no

# Get existing archives
echo "`date` List of archives before backup:"
echo "`borg list $REPOSITORY`"

# Backup
EMAILBODY="$(borg create -v --stats $REPOSITORY::$PREFIX-$BACKUPDATE $BACKUPDIR 2>&1)"
if [ $? -eq 0 ]
then
    EMAILSUBJECT="INFO: Automated backup succeeded for $PREFIX-$BACKUPDATE."
else
    EMAILSUBJECT=$WARNINGMSG
fi

# Prune. The prefix is important to limit prune's operation to this prefix's archives.
EMAILBODY="$EMAILBODY$(borg prune -v --list $REPOSITORY --prefix '$PREFIX-' --keep-daily=$KEEPDAILY --keep-weekly=$KEEPWEEKLY --keep-monthly=$KEEPMONTHLY 2>&1)"

echo "${EMAILBODY}"|mail -s "$EMAILSUBJECT" $EMAIL
echo "$EMAILBODY"

# Get resulting archives
echo "`date` List of archives after backup:"
echo "`borg list $REPOSITORY`"

PASSPHRASE=
export BORG_PASSPHRASE=$PASSPHRASE #clear password

# Umount backup disk
ssh $SSHHOST umount /dev/disk/by-uuid/$BACKUPUUID
echo "`date` Backup disk dismounted. Procedure finished."
echo ""

exit
